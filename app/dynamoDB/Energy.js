var AWS = require("aws-sdk");
var config = require("../config/aws.config");

AWS.config.update({
  region: config.region,
  endpoint: config.endpoint, 
  accessKeyId: config.aws_access_key_id,
  secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

var params = {
    TableName : 'HVAC_ts_1',
    ProjectionExpression:"#tm, #ratc, #spacetc, #satc, #oatc",
    FilterExpression: "#tm between :st_time and :end_time",
    ExpressionAttributeNames:{
        "#tm": "timestamp",
        "#monthly": "energy_monthly",
        "#weekly": "energy_weekly",
        "#yearly": "energy_yearly",
    },
    ExpressionAttributeValues: {
        ":end_time": "1566691200",
        ":st_time": "1564066800"
    }
};

docClient.scan(params, onScan);

var resultList = []

function onScan(err, data) {
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        // print all the movies
        console.log("Scan succeeded.");
        data.Items.forEach(function(item) {
           resultList.push(item);
        });

        // continue scanning if we have more movies, because
        // scan can retrieve a maximum of 1MB of data
        if (typeof data.LastEvaluatedKey != "undefined") {
            console.log("Scanning for more...");
            params.ExclusiveStartKey = data.LastEvaluatedKey;
            docClient.scan(params, onScan);
        }
    }
}

module.exports.HVACData = resultList;