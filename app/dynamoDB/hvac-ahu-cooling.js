var AWS = require("aws-sdk");
var config = require("../config/aws.config");

AWS.config.update({
  region: config.region,
  endpoint: config.endpoint, 
  accessKeyId: config.aws_access_key_id,
  secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

function setParams(timeArgument) {
    var result = {};
    result.currentAHUCoolingParams = {
        TableName : "HVAC_ahu_cooling",
        ProjectionExpression:"#tm, energy_weekly, energy_monthly, energy_yearly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp"
        },
        ExpressionAttributeValues: {
            ":timeArgument": timeArgument
        }
    }

    var previousWeeklyTimeArgument = (parseInt(timeArgument) - (3600*24*7)).toString();
    result.previousWeeklyAHUCoolingParams = {
        TableName : "HVAC_ahu_cooling",
        ProjectionExpression:"#tm, energy_weekly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousWeeklyTimeArgument
        }
    }

    var previousMonthlyTimeArgument = (parseInt(timeArgument) - (3600*24*30)).toString();
    result.previousMonthlyAHUCoolingParams = {
        TableName : "HVAC_ahu_cooling",
        ProjectionExpression:"#tm, energy_monthly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousMonthlyTimeArgument
        }
    }

    var previousYearlyTimeArgument = (parseInt(timeArgument) - (3600*24*365)).toString();
    result.previousYearlyAHUCoolingParams = {
        TableName : "HVAC_ahu_cooling",
        ProjectionExpression:"#tm, energy_yearly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousYearlyTimeArgument
        }
    }
    return result;
}

function ultimateQuery(timeArgument) {

    var params = setParams(timeArgument);

    var currentAHUCoolingRequest = docClient.query(params.currentAHUCoolingParams).promise();
    var previousWeeklyAHUCoolingRequest = docClient.query(params.previousWeeklyAHUCoolingParams).promise();
    var previousMonthlyAHUCoolingRequest = docClient.query(params.previousMonthlyAHUCoolingParams).promise();
    var previousYearlyAHUCoolingRequest = docClient.query(params.previousYearlyAHUCoolingParams).promise();
    
    return Promise.all([
        currentAHUCoolingRequest,
        previousWeeklyAHUCoolingRequest,
        previousMonthlyAHUCoolingRequest,
        previousYearlyAHUCoolingRequest
    ]).then(([currentData, previousWeekData, previousMothData, previousYearData]) => {
        return {
            currentData: currentData.Items,
            previousWeekData: previousWeekData.Items,
            previousMonthData: previousMothData.Items,
            previousYearData: previousYearData.Items
        };
    });
}

module.exports = {
    ultimateQuery: ultimateQuery
};