const db = require("../dynamoDB/overview");
const config = require("../config/common.config");
const ahu_cooling = require("../dynamoDB/hvac-ahu-cooling");
const ahu_heating = require("../dynamoDB/hvac-ahu-heating");
const heatpump_cooling = require("../dynamoDB/hvac-heatpump-cooling");
const heatpump_heating = require("../dynamoDB/hvac-heatpump-heating");


function checkExist(array, element, prop) {
    if(array[element]) {
        return array[element][prop];
    }
    return 0;
}

function AHUTotalEneryPerformance(req, res) {
    var heating_request = ahu_heating.ultimateQuery(config.timeArgument);

    var cooling_request = ahu_cooling.ultimateQuery(config.timeArgument);

    Promise.all([heating_request, cooling_request]).then(([heating, cooling]) => {
        var data = {
            currentData : {
                energy_yearly: (parseFloat(checkExist(heating.currentData, 0, "energy_yearly")) + parseFloat(checkExist(cooling.currentData, 0, "energy_yearly"))) / 1000,
                energy_weekly: (parseFloat(checkExist(heating.currentData, 0, "energy_weekly")) + parseFloat(checkExist(cooling.currentData, 0, "energy_weekly"))) / 1000,
                energy_monthly: (parseFloat(checkExist(heating.currentData, 0, "energy_monthly")) + parseFloat(checkExist(cooling.currentData, 0, "energy_monthly"))) / 1000,
                timestamp: parseFloat(checkExist(heating.currentData, 0, "timestamp"))
            },
            previousWeekData: {
                energy_weekly: (parseFloat(checkExist(heating.previousWeekData, 0, "energy_weekly")) + parseFloat(checkExist(cooling.previousWeekData, 0, "energy_weekly"))) / 1000,
                timestamp: parseFloat(checkExist(heating.previousWeekData, 0, "timestamp"))
            },
            previousMonthData: {
                energy_monthly: (parseFloat(checkExist(heating.previousMonthData, 0, "energy_monthly")) + parseFloat(checkExist(cooling.previousMonthData, 0, "energy_monthly"))) / 1000,
                timestamp: parseFloat(checkExist(heating.previousMonthData, 0, "timestamp"))
            },
            previousYearData: {
                energy_yearly: (parseFloat(checkExist(heating.previousYearData, 0, "energy_yearly")) + parseFloat(checkExist(cooling.previousYearData, 0, "energy_yearly"))) / 1000,
                timestamp: parseFloat(checkExist(heating.previousYearData, 0, "timestamp"))
            }

        }
        res.status(200).send(data)
    });
}

function HeatPumpTotalEneryPerformance(req, res) {
    var heating_request = heatpump_heating.ultimateQuery(config.timeArgument);

    var cooling_request = heatpump_cooling.ultimateQuery(config.timeArgument);

    Promise.all([heating_request, cooling_request]).then(([heating, cooling]) => {
        var data = {
            currentData : {
                energy_yearly: (parseFloat(checkExist(heating.currentData, 0, "energy_yearly_kw_hr")) + parseFloat(checkExist(cooling.currentData, 0, "energy_yearly_kw_hr"))) / 1000,
                energy_weekly: (parseFloat(checkExist(heating.currentData, 0, "energy_weekly_kw_hr")) + parseFloat(checkExist(cooling.currentData, 0, "energy_weekly_kw_hr"))) / 1000,
                energy_monthly: (parseFloat(checkExist(heating.currentData, 0, "energy_monthly_kw_hr")) + parseFloat(checkExist(cooling.currentData, 0, "energy_monthly_kw_hr"))) / 1000,
                timestamp: parseFloat(checkExist(heating.currentData, 0, "timestamp"))
            },
            previousWeekData: {
                energy_weekly: (parseFloat(checkExist(heating.previousWeekData, 0, "energy_weekly_kw_hr")) + parseFloat(checkExist(cooling.previousWeekData, 0, "energy_weekly_kw_hr"))) / 1000,
                timestamp: parseFloat(checkExist(heating.previousWeekData, 0, "timestamp"))
            },
            previousMonthData: {
                energy_monthly: (parseFloat(checkExist(heating.previousMonthData, 0, "energy_monthly_kw_hr")) + parseFloat(checkExist(cooling.previousMonthData, 0, "energy_monthly_kw_hr"))) / 1000,
                timestamp: parseFloat(checkExist(heating.previousMonthData, 0, "timestamp"))
            },
            previousYearData: {
                energy_yearly: (parseFloat(checkExist(heating.previousYearData, 0, "energy_yearly_kw_hr")) + parseFloat(checkExist(cooling.previousYearData, 0, "energy_yearly_kw_hr"))) / 1000,
                timestamp: parseFloat(checkExist(heating.previousYearData, 0, "timestamp"))
            }

        }
        res.status(200).send(data)
    });
}


function TotalEnergyPerformance(req, res) {
    var ahu_heating_request = ahu_heating.ultimateQuery(config.timeArgument);
    var ahu_cooling_request = ahu_cooling.ultimateQuery(config.timeArgument);
    var hp_heating_request = heatpump_heating.ultimateQuery(config.timeArgument);
    var hp_cooling_request = heatpump_cooling.ultimateQuery(config.timeArgument);
    Promise.all([ahu_heating_request, ahu_cooling_request, hp_heating_request, hp_cooling_request]).then(([ahu_heat, ahu_cool, hp_heat, hp_cool]) => {
        var data = {
            currentData : {
                energy_yearly: (
                    parseFloat(checkExist(hp_heat.currentData, 0, "energy_yearly_kw_hr")) + 
                    parseFloat(checkExist(hp_cool.currentData, 0, "energy_yearly_kw_hr")) +
                    parseFloat(checkExist(ahu_heat.currentData, 0, "energy_yearly")) +
                    parseFloat(checkExist(ahu_cool.currentData, 0, "energy_yearly"))
                    ) / 1000,
                energy_weekly: (
                    parseFloat(checkExist(hp_heat.currentData, 0, "energy_weekly_kw_hr")) + 
                    parseFloat(checkExist(hp_cool.currentData, 0, "energy_weekly_kw_hr")) +
                    parseFloat(checkExist(ahu_heat.currentData, 0, "energy_weekly")) +
                    parseFloat(checkExist(ahu_cool.currentData, 0, "energy_weekly"))
                    ) / 1000,
                energy_monthly: (
                    parseFloat(checkExist(hp_heat.currentData, 0, "energy_monthly_kw_hr")) + 
                    parseFloat(checkExist(hp_cool.currentData, 0, "energy_monthly_kw_hr")) +
                    parseFloat(checkExist(ahu_heat.currentData, 0, "energy_monthly")) + 
                    parseFloat(checkExist(ahu_cool.currentData, 0, "energy_monthly"))
                    ) / 1000,
                timestamp: parseFloat(checkExist(ahu_heat.currentData, 0, "timestamp"))
            },
            previousWeekData: {
                energy_weekly: (
                    parseFloat(checkExist(hp_heat.previousWeekData, 0, "energy_weekly_kw_hr")) + 
                    parseFloat(checkExist(hp_cool.previousWeekData, 0, "energy_weekly_kw_hr")) +
                    parseFloat(checkExist(ahu_heat.previousWeekData, 0, "energy_weekly")) + 
                    parseFloat(checkExist(ahu_cool.previousWeekData, 0, "energy_weekly"))
                    ) / 1000,
                timestamp: parseFloat(checkExist(ahu_heat.previousWeekData, 0, "timestamp"))
            },
            previousMonthData: {
                energy_monthly: (
                    parseFloat(checkExist(hp_heat.previousMonthData, 0, "energy_monthly_kw_hr")) + 
                    parseFloat(checkExist(hp_cool.previousMonthData, 0, "energy_monthly_kw_hr")) +
                    parseFloat(checkExist(ahu_heat.previousMonthData, 0, "energy_monthly")) + 
                    parseFloat(checkExist(ahu_cool.previousMonthData, 0, "energy_monthly"))
                    ) / 1000,
                timestamp: parseFloat(checkExist(ahu_heat.previousMonthData, 0, "timestamp"))
            },
            previousYearData: {
                energy_yearly: (
                    parseFloat(checkExist(hp_heat.previousYearData, 0, "energy_yearly_kw_hr")) + 
                    parseFloat(checkExist(hp_cool.previousYearData, 0, "energy_yearly_kw_hr")) +
                    parseFloat(checkExist(ahu_heat.previousYearData, 0, "energy_yearly")) + 
                    parseFloat(checkExist(ahu_cool.previousYearData, 0, "energy_yearly"))
                    ) / 1000,
                timestamp: parseFloat(checkExist(ahu_heat.previousYearData, 0, "timestamp"))
            }

        }
        res.status(200).send(data)
    });
}
exports.AHUTotalEneryPerfomance = AHUTotalEneryPerformance;

exports.HeatPumpTotalEneryPerfomance = HeatPumpTotalEneryPerformance;

exports.TotalEnergyPerformance = TotalEnergyPerformance;


