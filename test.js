var AWS = require("aws-sdk");
var config = require("./app/config/aws.config");


AWS.config.update({
    region: config.region,
    endpoint: config.endpoint,
    accessKeyId: config.aws_access_key_id,
    secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

var params = {
    TableName: "HVAC_ahu_cooling",
    ProjectionExpression: "#tm, energy_weekly, energy_monthly, energy_yearly",
    KeyConditionExpression: "#tm = :timeArgument",
    ExpressionAttributeNames: {
        "#tm": "timestamp"
    },
    ExpressionAttributeValues: {
        ":timeArgument": "1566691200"
    }
};

docClient.query(params, function (err, data) {
    if (err) {
        console.log("Unable to query. Error:", JSON.stringify(err, null, 2));
    } else {
        console.log("Query succeeded.");
        data.Items.forEach(function (item) {
            console.log(item)
        });
    }
});