const controller = require("../dynamoDB/overview");
const config = require("../config/common.config");


exports.overview = (req, res) => {
    var table = req.body.tableName;
    var temp = req.body.tempParams;
    var cop = req.body.cop;
    var data = controller.ultimateQuery(table, config.timeArgument, temp, cop)

    Promise.all([data]).then(([data]) => {
        res.status(200).send(data)
    })
};


/*
tableName = "HVAC_ahu_cooling" || "HVAC_ahu_heating" || "HVAC_heatpump_cooling" || "HVAC_heatpump_heating"
temp = "["ra_t_c", "space_t_c", "sa_t_c", "oa_t_c"]" || ["chw_supply_t_c", "chw_return_t_c", "chw_fr_l_s"]
cop = "dt_air_k" || "cop" 
*/ 