/** testing Authorization
 *  – /api/test/all for public access
    – /api/test/user for loggedin users (any role)
    – /api/test/mod for moderator users
    – /api/test/admin for admin users
 * **/
const db = require("../models");
const User = db.user;

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  User.find({}, function (err, users) {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(users);
    }
  });
  // res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};