var AWS = require("aws-sdk");
var config = require("../config/aws.config");

AWS.config.update({
    region: config.region,
    endpoint: config.endpoint,
    accessKeyId: config.aws_access_key_id,
    secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

function setParams(tableArgument, timeArgument, tempArgument, copArgument) {
    var cols_query = "";
    tempArgument.forEach(element => {
        cols_query += (element + ", ")
    });
    cols_query = cols_query.substring(0, cols_query.length - 2)
    var params = {
        tempParams: {
            TableName: tableArgument,
            // ProjectionExpression: "#tm, ra_t_c, space_t_c, sa_t_c, oa_t_c",
            ProjectionExpression: "#tm, " + cols_query,
            FilterExpression: "#tm between :end_time and :st_time",
            // KeyConditionExpression: "#tm between :end_time and :st_time",
            ExpressionAttributeNames: {
                "#tm": "timestamp",
            },
            ExpressionAttributeValues: {
                ":st_time": timeArgument,
                ":end_time": (parseInt(timeArgument) - 10 * 3600).toString(),
            }
        },
        copParams: {
            TableName: tableArgument,
            ProjectionExpression: "#tm, #cop",
            FilterExpression: "#tm between :end_time and :st_time",
            ExpressionAttributeNames: {
                "#tm": "timestamp",
                "#cop": copArgument
            },
            ExpressionAttributeValues: {
                ":st_time": timeArgument,
                ":end_time": (parseInt(timeArgument) - 10 * 3600).toString(),
            }
        }
    }
    return params;
}

function onScan(err, data) {
    if (err) {
        return err;
    } else {
        result = []
        data.Items.forEach((element) => {
           result.push(element);
        });
        // continue scanning if we have more movies, because
        // scan can retrieve a maximum of 1MB of data
        if (typeof data.LastEvaluatedKey != "undefined") {
            console.log("Scanning for more...");
            params.ExclusiveStartKey = data.LastEvaluatedKey;
            docClient.scan(params, onScan);
        }
        return result;
    }
}

function ultimateQuery(tableArgument, timeArgument, tempArgument, copArgument) {

    var params = setParams(tableArgument, timeArgument, tempArgument, copArgument);

    // var tempRequest = docClient.query(params.tempParams).promise();
    var tempRequest = docClient.scan(params.tempParams, onScan).promise();
    var copRequest = docClient.scan(params.copParams, onScan).promise();

    return Promise.all([
        tempRequest,
        copRequest
    ]).then(([temp, cop]) => {
        return {
            tempData: temp.Items,
            copData: cop.Items
        };
    });
}

module.exports = {
    ultimateQuery: ultimateQuery
};