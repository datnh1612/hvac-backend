var AWS = require("aws-sdk");
var config = require("../config/aws.config");

AWS.config.update({
  region: config.region,
  endpoint: config.endpoint, 
  accessKeyId: config.aws_access_key_id,
  secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

function setParams(timeArgument) {
    var result = {};
    result.currentAHUHeatingParams = {
        TableName : "HVAC_ahu_heating",
        ProjectionExpression:"#tm, energy_weekly, energy_monthly, energy_yearly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp"
        },
        ExpressionAttributeValues: {
            ":timeArgument": timeArgument
        }
    }

    var previousWeeklyTimeArgument = (parseInt(timeArgument) - (3600*24*7)).toString();
    result.previousWeeklyAHUHeatingParams = {
        TableName : "HVAC_ahu_heating",
        ProjectionExpression:"#tm, energy_weekly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousWeeklyTimeArgument
        }
    }

    var previousMonthlyTimeArgument = (parseInt(timeArgument) - (3600*24*30)).toString();
    result.previousMonthlyAHUHeatingParams = {
        TableName : "HVAC_ahu_heating",
        ProjectionExpression:"#tm, energy_monthly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousMonthlyTimeArgument
        }
    }

    var previousYearlyTimeArgument = (parseInt(timeArgument) - (3600*24*365)).toString();
    result.previousYearlyAHUHeatingParams = {
        TableName : "HVAC_ahu_heating",
        ProjectionExpression:"#tm, energy_yearly",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousYearlyTimeArgument
        }
    }
    return result;
}

function ultimateQuery(timeArgument) {
    var params = setParams(timeArgument);
    
    var currentAHUHeatingRequest = docClient.query(params.currentAHUHeatingParams, function(err, data) {}).promise();

    var previousWeekAHUHeatingRequest = docClient.query(params.previousWeeklyAHUHeatingParams, function(err, data) {}).promise();

    var previousMonthAHUHeatingRequest = docClient.query(params.previousMonthlyAHUHeatingParams, function(err, data) {}).promise();

    var previousYearAHUHeatingRequest = docClient.query(params.previousYearlyAHUHeatingParams, function(err, data) {}).promise();

    return Promise.all([
        currentAHUHeatingRequest,
        previousWeekAHUHeatingRequest,
        previousMonthAHUHeatingRequest,
        previousYearAHUHeatingRequest
    ]).then(([currentData, previousWeekData, previousMonthData, previousYearData]) => {
        return {
            currentData: currentData.Items,
            previousWeekData: previousWeekData.Items,
            previousMonthData: previousMonthData.Items,
            previousYearData: previousYearData.Items
        }
    })
}

module.exports = {
    ultimateQuery: ultimateQuery
};