var AWS = require("aws-sdk");
var config = require("../config/aws.config");

AWS.config.update({
  region: config.region,
  endpoint: config.endpoint, 
  accessKeyId: config.aws_access_key_id,
  secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

function setParams(timeArgument) {
    var result = {};
    result.currentHeatPumpHeatingParams = {
        TableName : "HVAC_heatpump_heating",
        ProjectionExpression:"#tm, energy_weekly_kw_hr, energy_monthly_kw_hr, energy_yearly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp"
        },
        ExpressionAttributeValues: {
            ":timeArgument": timeArgument
        }
    }

    var previousWeeklyTimeArgument = (parseInt(timeArgument) - (3600*24*7)).toString();
    result.previousWeeklyHeatPumpHeatingParams = {
        TableName : "HVAC_heatpump_heating",
        ProjectionExpression:"#tm, energy_weekly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousWeeklyTimeArgument
        }
    }

    var previousMonthlyTimeArgument = (parseInt(timeArgument) - (3600*24*30)).toString();
    result.previousMonthlyHeatPumpHeatingParams = {
        TableName : "HVAC_heatpump_heating",
        ProjectionExpression:"#tm, energy_monthly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousMonthlyTimeArgument
        }
    }

    var previousYearlyTimeArgument = (parseInt(timeArgument) - (3600*24*365)).toString();
    result.previousYearlyHeatPumpHeatingParams = {
        TableName : "HVAC_heatpump_heating",
        ProjectionExpression:"#tm, energy_yearly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousYearlyTimeArgument
        }
    }
    return result;
}

function ultimateQuery(timeArgument) {
    var params = setParams(timeArgument);
   
    var currentHeatPumpHeatingRequest = docClient.query(params.currentHeatPumpHeatingParams, function(err, data) {}).promise();

    var previousWeekHeatPumpHeatingRequest = docClient.query(params.previousWeeklyHeatPumpHeatingParams, function(err, data) {}).promise();

    var previousMonthHeatPumpHeatingRequest = docClient.query(params.previousMonthlyHeatPumpHeatingParams, function(err, data) {}).promise();

    var previousYearHeatPumpHeatingRequest = docClient.query(params.previousYearlyHeatPumpHeatingParams, function(err, data) {}).promise();

    return Promise.all([
        currentHeatPumpHeatingRequest,
        previousWeekHeatPumpHeatingRequest,
        previousMonthHeatPumpHeatingRequest,
        previousYearHeatPumpHeatingRequest
    ]).then(([currentData, previousWeekData, previousMonthData, previousYearData]) => {
        return {
            currentData: currentData.Items,
            previousWeekData: previousWeekData.Items,
            previousMonthData: previousMonthData.Items,
            previousYearData: previousYearData.Items
        }
    })
}

module.exports = {
    ultimateQuery: ultimateQuery
};