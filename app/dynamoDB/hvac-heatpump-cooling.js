var AWS = require("aws-sdk");
var config = require("../config/aws.config");

AWS.config.update({
  region: config.region,
  endpoint: config.endpoint, 
  accessKeyId: config.aws_access_key_id,
  secretAccessKey: config.aws_secret_access_key
});

var docClient = new AWS.DynamoDB.DocumentClient();

function setParams(timeArgument) {
    var result = {};
    result.currentHeatPumpCoolingParams = {
        TableName : "HVAC_heatpump_cooling",
        ProjectionExpression:"#tm, energy_weekly_kw_hr, energy_monthly_kw_hr, energy_yearly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp"
        },
        ExpressionAttributeValues: {
            ":timeArgument": timeArgument
        }
    }

    var previousWeeklyTimeArgument = (parseInt(timeArgument) - (3600*24*7)).toString();
    result.previousWeeklyHeatPumpCoolingParams = {
        TableName : "HVAC_heatpump_cooling",
        ProjectionExpression:"#tm, energy_weekly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousWeeklyTimeArgument
        }
    }

    var previousMonthlyTimeArgument = (parseInt(timeArgument) - (3600*24*30)).toString();
    result.previousMonthlyHeatPumpCoolingParams = {
        TableName : "HVAC_heatpump_cooling",
        ProjectionExpression:"#tm, energy_monthly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousMonthlyTimeArgument
        }
    }

    var previousYearlyTimeArgument = (parseInt(timeArgument) - (3600*24*365)).toString();
    result.previousYearlyHeatPumpCoolingParams = {
        TableName : "HVAC_heatpump_cooling",
        ProjectionExpression:"#tm, energy_yearly_kw_hr",
        KeyConditionExpression: "#tm = :timeArgument",
        ExpressionAttributeNames:{
            "#tm": "timestamp",
        },
        ExpressionAttributeValues: {
            ":timeArgument": previousYearlyTimeArgument
        }
    }
    return result;
}

function ultimateQuery(timeArgument) {
    var params = setParams(timeArgument);
   
    var currentHeatPumpCoolingRequest = docClient.query(params.currentHeatPumpCoolingParams, function(err, data) {}).promise();

    var previousWeekHeatPumpCoolingRequest = docClient.query(params.previousWeeklyHeatPumpCoolingParams, function(err, data) {}).promise();

    var previousMonthHeatPumpCoolingRequest = docClient.query(params.previousMonthlyHeatPumpCoolingParams, function(err, data) {}).promise();

    var previousYearHeatPumpCoolingRequest = docClient.query(params.previousYearlyHeatPumpCoolingParams, function(err, data) {}).promise();

    return Promise.all([
        currentHeatPumpCoolingRequest,
        previousWeekHeatPumpCoolingRequest,
        previousMonthHeatPumpCoolingRequest,
        previousYearHeatPumpCoolingRequest
    ]).then(([currentData, previousWeekData, previousMonthData, previousYearData]) => {
        return {
            currentData: currentData.Items,
            previousWeekData: previousWeekData.Items,
            previousMonthData: previousMonthData.Items,
            previousYearData: previousYearData.Items
        }
    })
}

module.exports = {
    ultimateQuery: ultimateQuery
};