const { authJwt } = require("../middlewares");
const controller = require("../controllers/hvac.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/hvac/total-ahu-performance", controller.AHUTotalEneryPerfomance);
  app.get("/api/hvac/total-heatpump-performance", controller.HeatPumpTotalEneryPerfomance);
  app.get("/api/hvac/total-performance", controller.TotalEnergyPerformance);
};